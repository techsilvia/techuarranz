var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');
//variables mongodb
var mongoClient = require('mongodb').MongoClient;
//var url ="mongodb://localhost:27017/local";
//varialbe url cambiar por nombre de la network si dockerizamos mongo
var url ="mongodb://servermongo:27017/local";

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=jEx6OlQOJmUHZeZBARrkqhO-eEeuOlTK";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + apiKey);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/getmongomovi', function(req, res){

mongoClient.connect(url, function(err, db){
if (err)
{
console.log(err);
}
else
{
var col = db.collection('movimientos');
console.log("Connected successfully to server");
col.find({}).limit(3).toArray(function(err, docs){
res.send(docs);
});
db.close();
res.send("ok");
}
})
});

/*
app.post('/postmongomovi', function(req, res) {
  mongoClient.connect(url,function(err, db){
    if (err)
    {
      console.log(err);
    }
    else
    {
    console.log("traying to insert to server");
    var col = db.collection('prueba');
    db.collection('prueba').insertOne({a:1}), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    /*
    db.collection('prueba').insertMany([{a:2,a:3}]), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    });

    db.collection('prueba').insertOne(req.body), function(err, r){
      console.log(r.insertedCount + 'registros insertados');
    });

    }
    db.close();
    res.send("ok");
  }
})
}); */

app.get('/movimientos', function(req, res) {
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      console.log(body);
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
  clienteMlab.post('',req.body,function(err, resM, body){
  if(err)
  {
    console.log(body);
  }
  else
  {
    res.send(body);
  }
});
});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
